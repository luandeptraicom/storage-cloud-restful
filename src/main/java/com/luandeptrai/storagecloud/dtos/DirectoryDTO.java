package com.luandeptrai.storagecloud.dtos;

import lombok.Data;

import java.util.Date;

@Data
public class DirectoryDTO {
    private long id;
    private String name;
    private long sizeBytes;
    private Date lastModifiedAt;
    private DirectoryDTO parent;
}
