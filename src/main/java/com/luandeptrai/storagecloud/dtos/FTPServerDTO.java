package com.luandeptrai.storagecloud.dtos;

import lombok.Data;

@Data
public class FTPServerDTO {
    private String id;
    private String hostName;
    private String hostAddress;
    private String username;
    private String password;
    private String encoding;
    private int port;
    private boolean ssl;
    private String encryption;
    private long timeOut;
    private String transferType;
    private boolean passiveMode;
}
