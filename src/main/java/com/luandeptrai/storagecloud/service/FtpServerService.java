package com.luandeptrai.storagecloud.service;

import com.luandeptrai.storagecloud.dtos.FTPServerDTO;

import java.util.List;

public interface FtpServerService {
    FTPServerDTO update(FTPServerDTO dto);

    List<FTPServerDTO> list(int page, int size);
}
