package com.luandeptrai.storagecloud.resources;


import com.luandeptrai.storagecloud.dtos.FTPServerDTO;
import com.luandeptrai.storagecloud.service.FtpServerService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ftp-server")
public class FtpServerResource {
    private final FtpServerService ftpServerService;

    public FtpServerResource(FtpServerService ftpServerService) {
        this.ftpServerService = ftpServerService;
    }

    @GetMapping("/")
    public List<FTPServerDTO> list(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ftpServerService.list(page, size);
    }

    @PutMapping("/")
    public FTPServerDTO update(@RequestBody FTPServerDTO dto) {
        return ftpServerService.update(dto);
    }
}
